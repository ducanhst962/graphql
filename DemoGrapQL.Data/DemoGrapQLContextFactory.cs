﻿using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;
using Microsoft.Extensions.Configuration;

namespace DemoGrapQL.Database
{
    public class DemoGrapQLContextFactory : IDesignTimeDbContextFactory<DemoGrapQLContext>
    {
        public DemoGrapQLContext CreateDbContext(string[] args)
        {
            var configuration = new ConfigurationBuilder()
                .SetBasePath(Directory.GetCurrentDirectory())
                .AddJsonFile("appsettings.json")
                .Build();

            var builder = new DbContextOptionsBuilder<DemoGrapQLContext>();
            var connectionString = configuration.GetConnectionString("DemoGrapQLDb");
            builder.UseSqlServer(connectionString);

            return new DemoGrapQLContext(builder.Options);
        }
    }
}
